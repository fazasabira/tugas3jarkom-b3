# Tugas3Jarkom-B3

Nama anggota:  <br />
Faza Siti Sabira Prakoso <br />
Patricia Anugrah Setiani <br />
Jeremy <br />

Fitur yang dikerjakan:  <br />
<br />
_**Fitur Dasar**_  <br />
- Adanya satu master node yang dapat menerima job dari user. Laptop/PC mahasiswa difungsikan sebagai Master node dan job dapat langsung di submit dari sini. <br />
- Adanya (minimal) satu worker node yang dapat mengeksekusi job dari master. Virtual machine (EC2) difungsikan sebagai worker node(s). <br />
- Adanya komunikasi antara master dan worker node(s). Master dapat mengirimkan perintah dan job kepada worker dan worker dapat mengirimkan balik hasil eksekusi kepada master. <br /> <br />
_**Fitur Elective**_  <br />
- Adanya antrian jobs di master node berdasarkan algoritma FCFS. <br />






