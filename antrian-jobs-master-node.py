import socket
import sys

HOST, PORT = "100.26.201.110", 10699
data = " ".join(sys.argv[1:])

while True:
    request_count = input('Please input request count: ')
    requests = []
    for x in range(int(request_count)):
        requests.append(input('Please input request ' + str(x) + ': '))
    for request in requests:
        # Create a socket (SOCK_STREAM means a TCP socket)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # Connect to server and send data
            sock.connect((HOST, PORT))
            sock.sendall(bytes(request + "\n", "utf-8"))

            # Receive data from the server and shut down
            received = str(sock.recv(1024), "utf-8")

        print("Sent:     {}".format(request))
        print("Received: {}".format(received))
